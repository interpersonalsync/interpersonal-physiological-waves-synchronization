const express = require("express");
const fileUpload = require("express-fileupload");
const path = require('path');
const cors = require('cors')
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const jwt = require('jsonwebtoken')
require('dotenv/config');
const app = express();
const data_chart = require("./handleData/create_data_for_chart.js")
const keysHandle = require("./handleData/extract_data_from_files")
const handle_zip = require("./handleData/handle_zip")
const CCF = require("./algorithms/CCF")
const computeSync = require("./handleData/computeSync")
var zip = require('underscore/cjs/zip.js');
const MDRQA = require('./algorithms/MDQRA')
const decompress = require("decompress");




const PORT = process.env.PORT || 5000;

app.use(fileUpload());

// Import routes
const usersRoute = require('./routes/users');
const bigExpRoute = require('./routes/big_experiments');
const authRoute = require('./routes/auth');
const expRoute = require('./routes/experiments');


const corsOptions = {
  origin:'http://localhost:8081',
  credentials:true,
  optionSuccessStatus:200
}

app.use(cors(corsOptions));

app.use(bodyParser.json());
app.use("/users", usersRoute);
// app.use("/bigexp", authenticateToken, bigExpRoute.router);
app.use("/bigexp", bigExpRoute.router);
app.use("/auth", authRoute);
app.use("/exp", expRoute.router);


// connect to DB
mongoose.connect(process.env.DB_CONNECTION, ()=> console.log('connected to DB'));
let fileName = "";

///////////////////////////////////////////////////////////////////////////////////////

// const noaRoute = require('./routes/noa')
// app.use("/noa", noatries, noaRoute);


const gmailRoute = require('./routes/google_mail');
app.use('/sendgmail', gmailRoute);

////////// example of how to run MDRQA algoritm
/*
// var nj = require('numjs');
// const mdrqa = require('./algorithms/MDQRA');
// nj.config.printThreshold = 9;
// let X = nj.arange(42).reshape(14,3);
// // let X = nj.arange(64).reshape(8,8);
// console.log("X = "+ X);

// let threshold = 50;
// let data = X.tolist();
// emb = 3;
// delay = 3;
// norm = 'euc';
// threshold = 50;
// minSeq = 2;
// Zscore = 0

// let Results = mdrqa(data, emb, delay, norm, threshold, minSeq, Zscore);
// let error = Results[0];
// if (error){
//     let message = Results[1];
//     console.log(message);
// }
// else{
//     let rec_plot = Results[2];
//     console.log("resulted recurrent plot: \n" + rec_plot);
//     let calcs = Results[3];
//     console.log("rec plot size = "+ calcs[0] +", REC = "+calcs[1] +", DET = "+calcs[2]+", Ratio = "+calcs[3]+", Lmax = "+calcs[4]+", Lmean = "+calcs[5]+", LAM = "+ calcs[6]+", Vmax = "+ calcs[7]+", Vmean = "+ calcs[8]);
// }


const gmailRoute = require('./routes/google_mail');
app.use('/sendgmail', gmailRoute);

////////// example of how to run MDRQA algoritm

// var nj = require('numjs');
// const mdrqa = require('./algorithms/MDQRA');
// nj.config.printThreshold = 9;
// let X = nj.arange(42).reshape(14,3);
// // let X = nj.arange(64).reshape(8,8);
// console.log("X = "+ X);

// let threshold = 50;
// let data = X.tolist();
// emb = 3;
// delay = 3;
// norm = 'euc';
// threshold = 50;
// minSeq = 2;
// Zscore = 0

// let Results = mdrqa(data, emb, delay, norm, threshold, minSeq, Zscore);
// let error = Results[0];
// if (error){
//     let message = Results[1];
//     console.log(message);
// }
// else{
//     let rec_plot = Results[2];
//     console.log("resulted recurrent plot: \n" + rec_plot);
//     let calcs = Results[3];
//     console.log("rec plot size = "+ calcs[0] +", REC = "+calcs[1] +", DET = "+calcs[2]+", Ratio = "+calcs[3]+", Lmax = "+calcs[4]+", Lmean = "+calcs[5]+", LAM = "+ calcs[6]+", Vmax = "+ calcs[7]+", Vmean = "+ calcs[8]);
// }

const {School, Person, Class} = require('./models/Try');

app.post('/getToken/:personId', async(req, res)=>{
  const user = await Person.findById(req.params.personId);
  console.log(user);
  const token = user.generateAuthToken();
  console.log(token)
  res.status(200).send({data: {token}, message:"Logged in successfully"});
})

function noatries(req, res, next){
  const token = req.query.token
  jwt.verify(token, process.env.JWTPRAIVTEKEY, (err, user)=>{
    if(err) return res.sendStatus(403)
    console.log(user)
    next()
  })
  // req.body.addedinfo = "noa";
  // req.jimbojy = {name:"noa", says:"hey"}
  // console.log("this worked!");
  // next();
}
*/

///////////////////////////////////////////////////////////////////////////////////////////

// TODO: make sure that all requests to /bigexp has in header an authorization field with format of: Bearer token
// Authenticate token
function authenticateToken(req, res, next){
  const authHeader = req.headers['authorization']
  const token = authHeader && authHeader.split(' ')[1]
  if (token == null) return res.sendStatus(401)
  jwt.verify(token, process.env.JWTPRAIVTEKEY, (err, userId)=>{
    if(err) return res.sendStatus(403)
    req.userId = userId
    next()
  })
}

const unZipFiles = () => {
    let zipPath = "../client/src/videos/" + fileName;
    decompress(zipPath, "../client/src/videos").then(files => {})
}



// Upload Endpoint from upload page
app.post('/upload', async (req, res) => {
  if (req.files === null) {
    return res.status(400).json({ msg: 'No file uploaded' });
  }
  try {
    const files = req.files.file;
    fileName = files.name;
    var savePath = undefined
    // in case multiple file uploaded
    if(files.length >= 2){
      let promises = [];
      files.forEach(file => {
        savePath = path.join(__dirname,'server_uploads', files.name)
        promises.push(file.mv(savePath))
      })
      await Promise.all(promises);
    }
    // in case one file uploaded
    else{
      savePath = path.join(__dirname,'server_uploads', files.name)
      await files.mv(savePath)
    }

    const bigExpData = await handle_zip(files.name, req.body.researcher)
    bigExpRoute.add_big_exp(bigExpData)
    res.json({ fileName: files.name, filePath: savePath, expResults: bigExpData });

  } catch (error) {
    console.error(error);
    return res.status(500).send(error);
  }

});

// Upload Endpoint from select page
app.post('/selectExp', async (req, res) => {
  if (req.files === null) {
    return res.status(400).json({ msg: 'No file uploaded' });
  }
  try {
    const files = req.files.file;
    fileName = files.name;
    var savePath = undefined
    // in case multiple file uploaded
    if(files.length >= 2){
      let promises = [];
      files.forEach(file => {
        savePath = path.join('../client/src/videos', files.name)
        promises.push(file.mv(savePath))
      })
      await Promise.all(promises);
    }
    // in case one file uploaded
    else{
      savePath = path.join('../client/src/videos', files.name)
      await files.mv(savePath)
    }
    res.json({ fileName: files.name, filePath: savePath});

  } catch (error) {
    console.error(error);
    return res.status(500).send(error);
  }

});


// need to update the post request with the changes in the function and then dekete thr function
/*

async function mdr(){
  const expId = '624428907ba1d2e1d8f1315e'
  const userId = '621b9a9df96c67caf62f63bf'
  const exp_part = "BL"
  const measure = "HRV"

  var expData = await expRoute.getGroupResults(userId, expId)
  expData = keysHandle.add_keys_to_mongo_scheme(expData[0])
  var personsVectors = expData["parts"][exp_part]["measures_vec"][measure]["persons_vectors"]
  var input = []
  var minlen = personsVectors[0]["measure_values"].length
  personsVectors.forEach(person => {
    if(person["measure_values"].length < minlen) {
      minlen = person["measure_values"].length
    }
    input.push(person["measure_values"])
  });
  for(let i = 0; i < input.length; i++){
    input[i] = input[i].slice(0,minlen)

  }
  // transpose the array
  input = input[0].map((_, colIndex) => input.map(row => row[colIndex]));

  let threshold = 50;
  emb = 3;
  delay = 3;
  norm = 'euc';
  threshold = 50;
  minSeq = 2;
  Zscore = 0

  var results = MDRQA(input, emb, delay, norm, threshold, minSeq, Zscore)
  // let error = results[0];

  // if(!error){
  let rec_plot = results[2];
  console.log("resulted recurrent plot: \n" + rec_plot);
  let calcs = results[3];
  console.log("rec plot size = "+ calcs[0] +", REC = "+calcs[1] +", DET = "+calcs[2]+", Ratio = "+calcs[3]+", Lmax = "+calcs[4]+", Lmean = "+calcs[5]+", LAM = "+ calcs[6]+", Vmax = "+ calcs[7]+", Vmean = "+ calcs[8]);
  
  // }


}
mdr()


app.post("/mdrqa", async (req, res) => {

  const userId = req.body.userId
  const expId = req.body.expId
  const exp_part = req.body.exp_part
  const measure = req.body.measure

  try{
    var expData = await expRoute.getGroupResults(userId, expId)
    expData = keysHandle.add_keys_to_mongo_scheme(expData[0])
    var personsVectors = expData["parts"][exp_part]["measures_vec"][measure]["persons_vectors"]
    var input = []
    personsVectors.forEach(person => {
      input.push(person["measure_values"])
    });
    
    var output = MDRQA(personsVectors)

    res.json({output: output});

  } catch (error) {
    console.error(error);
    return res.status(500).send(error);
  }
});

*/

app.post("/chart", async (req, res) => {

  const userId = req.body.userId
  const expId = req.body.expId
  const exp_part = req.body.exp_part
  const measure = req.body.measure

  try{
    var expData = await expRoute.getGroupResults(userId, expId)
    expData = keysHandle.add_keys_to_mongo_scheme(expData[0])  
    var chart = undefined
    chart = data_chart.extract_measure_values(expData,exp_part,measure)

    res.json({chart: chart});

  } catch (error) {
    console.error(error);
    return res.status(500).send(error);
  }
});


app.post("/groupInfo", async (req, res) => {
  const userId = req.body.userId
  const expId = req.body.expId
  try{
    var expData = await expRoute.getGroupResults(userId, expId)
    const info = []
    expData[0].parts.map(part => {
      var part_length = part["measures_vec"][0]["persons_vectors"][0]["measure_values"].length
      var part_name = part["part_name"]
      info.push({part_name: part_name, part_length: part_length })
    })

    const personsNames = []
    var personsVector = expData[0]["parts"][0]["measures_vec"][0]["persons_vectors"]
    personsVector.forEach(person => {
      personsNames.push(person["person_num"])
    });
    res.json({info: info, personsNames:  personsNames})

  } catch (error) {
    console.error(error);
    return res.status(500).send(error);
  }  

});

app.post("/sync", async (req, res) => {
  const userId = req.body.userId
  const expId = req.body.expId
  const exp_part = req.body.exp_part
  const measure = req.body.measure
  const window_size = parseInt(req.body.window_size)
  const shift = 6
  if(req.body.couples == ""){
    res.json({sync: {}})
    return null
  }
  const couples = req.body.couples.split(',').map(couple => couple.split('-'))

  // function for averaging arrays
  const avgEmAll = arrays => zip.apply(null, arrays).map(avg)
  const sum = (y, z) => Number(y) + Number(z)
  const avg = x => (x.reduce(sum) / x.length).toFixed(4) 


  try{
    var expData = await expRoute.getGroupResults(userId, expId)
    expData = keysHandle.add_keys_to_mongo_scheme(expData[0]) 

    var len = expData["parts"][exp_part]["measures_vec"][measure]["persons_vectors"][0]["measure_values"].length 
    const lines =[]
    const lines_names = []
    var syncArrays = []
    // in case of triplets/more it computes each couple and average them
    couples.forEach(couple => {
      var names = {}
      var count= 0
      for (var i = 0; i < couple.length; i++) {
        for (var j = i + 1; j < couple.length; j++) {
          var per1 = expData["parts"][exp_part]["measures_vec"][measure]["persons_vectors"][couple[i]]
          var per2 = expData["parts"][exp_part]["measures_vec"][measure]["persons_vectors"][couple[j]]
          names[per1["person_num"]] = 1
          names[per2["person_num"]] = 1
          const syncArray =  computeSync.getSyncArray(per1["measure_values"],per2["measure_values"], window_size,shift)
          syncArrays.push(syncArray)

        }
      }
      var sum_sync = syncArrays.length > 1 ? avgEmAll(syncArrays) : syncArrays[0]

      lines_names.push(Object.keys(names).join('-'))
      lines.push(sum_sync)

    });

    const x_values = data_chart.create_x_values(lines[0].length)
    var chart = data_chart.create_data(lines_names,lines,x_values)


    res.json({sync: chart})
  } catch (error) {
    console.error(error);
    return res.status(500).send(error);
  }
});

app.get("/unzip", (req, res) => {
  unZipFiles(fileName)
});

app.get("/analyze", (req, res) => {
  res.json({ message: "this is our analyzing page!" });
});

app.post("/hey", (req, res)=>{
  console.log(req.body);
});

app.listen(PORT, () => {
  console.log(`Server listening on ${PORT}`);
});
